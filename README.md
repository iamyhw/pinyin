# pinyin

#### 介绍
一个微小高效的拼音输入法,适用于GB2312码.方便移植到嵌入式系统;
该pinyin输入法分别管理字典和词典,如果需要更小的代码尺寸,可以只
使用字典,关闭词典。

字典对应的是一个字的拼音的GB2312编码汉字,每次只能检索一个汉字
词典对应的是不同的拼音对应的词条,长度和数量没有限制,可灵活增删。

该pinyin输入法的简单逻辑是,首先根据输入的首字母确定一个字母字典,
每当输入一个字母,都优先从字典中检索;字母字典只检索一个汉字的完
整拼音,但是优点是代码体积小,在代码尺寸要求比较高时可以只使用字
母字典。
如果字母字典找不到,再去检索词典;词典可以实现任意拼音或字母的组
合,代码中只录入了少量的示例词语,用户可以根据自己的应用场景录入一
些专业词语,这样既可提高输入效率,又能减小代码尺寸。

#### 应用
第一步,定义一个ime_pinyin_t类型的变量。
例如。
```
static ime_pinyin_t myime。
```
第二步,初始化myime。
```
pinyin_init(&myime);
```
第三步,调用以下接口录入一个字母;c只接受a-z,A-Z。
```
pinyin_getin_letter(&myime, c)。
```
例如。
```
pinyin_getin_letter(&myime, 'a')。
```
执行这个函数后,与字母a有关的汉字已经拷贝到myime下。
inbuf中存储的是输入的字母a。
wordstr存储的是当前的一个页,显示内容如下。
```
1.安 2.按 3.爱 4.阿 5.暗 6.啊 7.挨 8.碍 9凹。
```
如果有GUI系统,就可以用label控件显示这两条内容。
如果字比较多,会有多个page。
page_numbs表示有几个page。
page_index表示当前的是哪个page。
用户可以直接更改page_index的值实现翻页,再调用
```
pinyin_generate_page(&myime);
```
实现页面内容更新。

左翻页示例代码。
```
if (ime->page_numbs > 1)
{
    if (ime->page_index > 0)
	ime->page_index -= 1;
    pinyin_generate_page(ime);
    //此处可显示wordstr中的内容到GUI
}
```
右翻页示例代码。
```
if (ime->page_numbs > 1)
{
    if ((ime->page_index + 1) < ime->page_numbs)
        ime->page_index += 1;
    pinyin_generate_page(ime);
    //此处可显示wordstr中的内容到GUI
}
```
如果需要取消一个输入的字母,调用函数。
```
pinyin_backspace(&myime);
```
当需要选中一个字或词时,示例代码。
```
unsigned short zh[16] = {0};
pinyin_getout_zhstring(&myime, zh, 4);
```
以上面的wordstr内容为例进行说明。
```
1.安 2.按 3.爱 4.阿 5.暗 6.啊 7.挨 8.碍 9凹。
```
zh[]中会返回"暗",因为是编号从0开始的。

如果想方便的处理page中的每个汉字或词语,也可以直接使
用数组myime.s,数组s中保存了page中的字或词。
```
s[0]安 s[1]按 s[2]爱 s[3]阿 s[4]暗 s[5]啊 s[6]挨 s[7]碍 s[8]凹。
```
#### 结构体成员说明
###### 结构体struct ime_pinyin说明。
```
type;//表示检索的是字典还是词典。
*dicts;//当前字典。
*terms;//当前词典。
inbuf;//保存了输入的ascii字符串。
*zhptr;//保存了当前字典或词典的数据指针。
wordcnt;//反映了字/词典中每个字词的长度。
        //如果检索的是字典wordcnt=1。
        //如果检索的是词典wordcnt=struct rtgui_terms下的cnt。
inlen;//保存了输入的ascii字符串长度。
zhlen;//保存了检索到的字/词的个数。
cnt_per_page;//保存了一个page中可以显示多少个词。
cnt_cur_page;//保存了当前page中有多少个词。
page_index;//记录当前page编号。
page_numbs;//记录总的page数量。
wordstr;//由当前page生成的带编号的待选词字符串,用于GUI显示。
```